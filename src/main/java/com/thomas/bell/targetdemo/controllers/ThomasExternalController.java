package com.thomas.bell.targetdemo.controllers;

import java.util.Optional;

import com.thomas.bell.targetdemo.interfaces.PriceRepository;
import com.thomas.bell.targetdemo.models.Price;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ThomasExternalController {

    // MongoDB repo
    @Autowired
    private PriceRepository priceRepository;

    @GetMapping("/prices/{id}")
    public Price getPriceById(@PathVariable("id") String id) {

        Optional<Price> returnPrice = priceRepository.findById(Long.valueOf(id));

        return returnPrice.get();
    }

    @PutMapping("/prices/{id}")
    public String updatePrice(@PathVariable("id") String id, @RequestBody Price price) {

        priceRepository.save(price);

        return "Success";
    }

    @PostMapping("/prices/delete-all")
    public String deleteAllPrices() {

        priceRepository.deleteAll();

        return "Success";
    }
    
}