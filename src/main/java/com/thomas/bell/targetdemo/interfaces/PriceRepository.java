package com.thomas.bell.targetdemo.interfaces;

import java.util.Optional;

import com.thomas.bell.targetdemo.models.Price;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PriceRepository extends MongoRepository<Price, String> {

    public Optional<Price> findById(Long id);
    
}
